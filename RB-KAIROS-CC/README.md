# modern

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```
#
##Server Simulation Setup

### In the ROSDevelopmenet Studio
```
cd ~/catkin_ws
   source devel/setup.bash
   rospack profile
   roslaunch course_web_dev_ros web.launch
```
```
roslaunch my_summit_navigation main_lite.launch
```

### Get the Rosbridge
```
rosbridge_address
```


